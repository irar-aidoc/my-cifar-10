import os
import pickle

import numpy as np
from PIL import Image
from tensorflow.python import keras


def load_CIFAR_batch(filename):
    """ load single batch of cifar """
    with open(filename, 'rb') as f:
        datadict = pickle.load(f, encoding='latin1')
        X = datadict['data']
        Y = datadict['labels']
        X = X.reshape(10000, 3, 32, 32).transpose(0, 2, 3, 1).astype("float")
        Y = np.array(Y)
        return X, Y


def load_CIFAR10(ROOT):
    """ load all of cifar """
    xs = []
    ys = []
    for b in range(1, 6):
        f = os.path.join(ROOT, 'data_batch_%d' % (b,))
        X, Y = load_CIFAR_batch(f)
        xs.append(X)
        ys.append(Y)
    Xtr = np.concatenate(xs)
    Ytr = np.concatenate(ys)
    del X, Y
    Xte, Yte = load_CIFAR_batch(os.path.join(ROOT, 'test_batch'))
    return Xtr, Ytr, Xte, Yte


def get_cifar10_data_from_stanford(num_training=49000, num_validation=1000, num_test=1000,
                                   subtract_mean=True):
    """
    Load the CIFAR-10 dataset from disk and perform preprocessing to prepare
    it for classifiers. Copied and modified from assignment2/cs231n/data_utils.py
    """
    # Load the raw CIFAR-10 data
    cifar10_dir = 'datasets/cifar-10-batches-py'
    X_train, y_train, X_test, y_test = load_CIFAR10(cifar10_dir)

    # Subsample the data
    mask = list(range(num_training, num_training + num_validation))
    X_val = X_train[mask]
    y_val = y_train[mask]
    mask = list(range(num_training))
    X_train = X_train[mask]
    y_train = y_train[mask]
    mask = list(range(num_test))
    X_test = X_test[mask]
    y_test = y_test[mask]

    mean_image = None
    # Normalize the data: subtract the mean image
    if subtract_mean:
        mean_image = np.mean(X_train, axis=0)
        X_train -= mean_image
        X_val -= mean_image
        X_test -= mean_image

    # Package data into a dictionary
    data_struct = {
        'X_train': X_train, 'y_train': y_train,
        'X_val': X_val, 'y_val': y_val,
        'X_test': X_test, 'y_test': y_test,
    }

    if mean_image is not None:
        data_struct['mean_image'] = mean_image

    return data_struct


def get_cifar10_data_fast():
    data_pickle_path = 'datasets/pickled_data'
    if os.path.exists(data_pickle_path):
        with open(data_pickle_path, 'rb') as f:
            data = pickle.load(f)
    else:
        data = get_cifar10_data_from_stanford()
        with open(data_pickle_path, 'wb') as f:
            pickle.dump(data, f)
    return data


def save_as_files():
    file_extension = '.png'
    files_path = 'datasets/as_files'
    data = get_cifar10_data_from_stanford(subtract_mean=False)
    x_train, y_train = data['X_train'], data['y_train']
    x_val, y_val = data['X_val'], data['y_val']
    x_test, y_test = data['X_test'], data['y_test']
    num_labels = len(np.unique(y_train))

    image_indices = np.zeros(num_labels, dtype=int)
    for images, labels, data_type in ((x_train, y_train, 'train'), (x_val, y_val, 'val'), (x_test, y_test, 'test')):
        data_type_path = os.path.join(files_path, data_type)
        os.makedirs(data_type_path, exist_ok=True)
        for i in range(num_labels):
            class_path = os.path.join(data_type_path, str(i))
            os.makedirs(class_path, exist_ok=True)
        for image, label in zip(images, labels):
            image_name = str(image_indices[label]) + file_extension
            image_indices[label] += 1
            image_path = os.path.join(files_path, data_type, str(label), image_name)
            Image.fromarray(image.astype(np.uint8)).save(image_path)


def dict_to_vars(data_dict):
    x_train, y_train = data_dict['X_train'], keras.utils.to_categorical(data_dict['y_train'])
    x_val, y_val = data_dict['X_val'], keras.utils.to_categorical(data_dict['y_val'])
    x_test, y_test = data_dict['X_test'], keras.utils.to_categorical(data_dict['y_test'])
    return x_train, y_train, x_val, y_val, x_test, y_test