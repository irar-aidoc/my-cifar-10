import os
import pickle
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
from keras.models import load_model
from tensorflow import keras

import data_utils

LOG_PATH = 'training_logs'
CLASSES = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']


def get_time_string():
    return str(datetime.now().isoformat('_').split('.')[0])


def save(training_history):
    dir_path = os.path.join(LOG_PATH, get_time_string())
    os.makedirs(dir_path)
    training_history.model.save(os.path.join(dir_path, 'model'))
    with open(os.path.join(dir_path, 'history'), 'wb') as f:
        pickle.dump(training_history.history, f)
    print('model saved at "{}"'.format(dir_path))


def load_and_plot_history(dir_path):
    with open(os.path.join(dir_path, 'history'), 'rb') as f:
        history = pickle.load(f)
    plot_results(history)
    return history


def plot_results(train_history, accuracy=True, loss=False):
    now = get_time_string()
    if accuracy:
        # summarize history for accuracy
        plt.plot(train_history['acc'], marker='o', linestyle='none')
        plt.plot(train_history['val_acc'], marker='o', linestyle='none')
        plt.title('model {}, top val acc: {}'.format(now, format(max(train_history['val_acc']))))
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='lower right')
        plt.show()
    if loss:
        # summarize history for loss
        plt.plot(train_history['loss'], marker='o', linestyle='none')
        plt.plot(train_history['val_loss'], marker='o', linestyle='none')
        plt.title('model loss for model {}'.format(now))
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper right')
        plt.show()


def create_model_image(model):
    keras.utils.plot_model(model, show_shapes=True)


def evaluate_model(dir_path, visualize=True):
    os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    model = load_model(os.path.join(dir_path, 'model'))
    __, __, __, __, x_test, y_test = data_utils.get_cifar10_data()
    print('evaluating model at {}:'.format(dir_path))
    loss, test_acc = model.evaluate(x_test, y_test)
    history = load_and_plot_history(dir_path)
    train_acc, val_acc = max(history['acc']), max(history['val_acc'])
    print('train accuracy: {}, validation accuracy: {}, test accuracy: {}'.format(train_acc, val_acc, test_acc))


def visualize_model(dir_path):
    os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    rows, columns = 5, 5
    num_samples = rows * columns
    model = load_model(os.path.join(dir_path, 'model'))
    _, _, _, _, x_test, y_test, mean_image = data_utils.get_cifar10_data(return_mean_image=True)
    sample_indices = np.random.choice(len(x_test), num_samples)
    sample_images = x_test[sample_indices]
    sample_labels = np.argmax(y_test[sample_indices], axis=1)
    predictions = np.argmax(model.predict(sample_images), axis=1)
    sample_labels_names = [CLASSES[label] for label in sample_labels]
    predicted_labels_names = [CLASSES[prediction] for prediction in predictions]
    sample_images += mean_image
    fig = plt.figure(figsize=(8, 8))
    for i in range(1, columns * rows + 1):
        fig.add_subplot(rows, columns, i)
        plt.imshow(sample_images[i - 1])
        plt.xticks([])
        plt.yticks([])
        plt.text(0, 35, s="true: {}, predicted: {}".format(sample_labels_names[i - 1], predicted_labels_names[i - 1]))
    plt.show()
