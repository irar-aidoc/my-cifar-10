import keras
from keras import regularizers, optimizers
from keras.layers import Add, Conv2D, Dense, Input, BatchNormalization, MaxPool2D, Flatten, AveragePooling2D, Dropout
from keras.models import Model


def get_model(model_type='cnn'):
    if model_type == 'cnn':
        model = get_cnn_model()
    elif model_type == 'residual_cnn':
        model = get_residual_cnn_model()
    else:
        raise ValueError('Unknown model type: {}'.format(model_type))
    model.compile(optimizer=optimizers.Adam(), loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def get_cnn_model(input_shape=(32, 32, 3)):
    inputs = Input(shape=input_shape)
    x = Conv2D(filters=32, kernel_size=3, activation="relu", input_shape=(32, 32, 3), padding='same')(inputs)
    x = BatchNormalization()(x)
    x = Conv2D(filters=32, kernel_size=3, activation="relu", input_shape=(32, 32, 3), padding='same')(x)
    x = BatchNormalization()(x)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = MaxPool2D((2, 2))(x)
    x = Dropout(0.2)(x)
    x = Conv2D(filters=64, kernel_size=3, activation="relu", padding='same')(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=64, kernel_size=3, activation="relu", padding='same')(x)
    x = BatchNormalization()(x)
    x = resblock(x, num_filters=64, kernel_size=3)
    x = resblock(x, num_filters=64, kernel_size=3)
    x = resblock(x, num_filters=64, kernel_size=3)
    x = MaxPool2D((2, 2))(x)
    x = Dropout(0.3)(x)
    x = Conv2D(filters=128, kernel_size=3, activation="relu", padding='same')(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=128, kernel_size=3, activation="relu", padding='same')(x)
    x = BatchNormalization()(x)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = MaxPool2D((2, 2))(x)
    x = Dropout(0.4)(x)
    x = Flatten()(x)
    x = Dense(128, activation='relu')(x)
    x = BatchNormalization()(x)
    x = Dropout(0.5)(x)
    predictions = Dense(10, activation='softmax')(x)
    return Model(inputs=inputs, outputs=predictions)


def get_residual_cnn_model():
    reg = regularizers.l2(l=1e-1)
    inputs = Input(shape=(32, 32, 3))
    x = Conv2D(filters=32, kernel_size=3, activation="relu", input_shape=(32, 32, 3), padding='same')(inputs)
    x = BatchNormalization()(x)
    x = MaxPool2D((2, 2), padding='same')(x)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = resblock(x, num_filters=32, kernel_size=3)
    x = MaxPool2D((2, 2), padding='same')(x)
    x = resblock(x, num_filters=128, kernel_size=3, downsample=True)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = resblock(x, num_filters=128, kernel_size=3)
    x = AveragePooling2D()(x)
    x = Flatten()(x)
    x = Dense(64, activation='relu', kernel_regularizer=reg)(x)
    predictions = Dense(10, activation='softmax')(x)
    return Model(inputs=inputs, outputs=predictions)


def resblock(x, num_filters, kernel_size, downsample=False):
    strides = 2 if downsample else 1
    fx = Conv2D(num_filters, kernel_size, activation='relu', strides=strides, padding='same')(x)
    fx = BatchNormalization()(fx)
    fx = Conv2D(num_filters, kernel_size, padding='same')(fx)
    if downsample:
        x = Conv2D(kernel_size=1, strides=2, filters=num_filters, padding="same")(x)
        x = BatchNormalization()(x)
    out = Add()([x, fx])
    out = keras.layers.Activation(keras.activations.relu)(out)
    out = BatchNormalization()(out)
    return out
