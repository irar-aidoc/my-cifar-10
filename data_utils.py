import numpy as np
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import to_categorical


def image_batch_to_vector_batch(image_batch):
    return image_batch.reshape(image_batch.shape[0], -1)


def get_cifar10_data(return_mean_image=False):
    (x_train, y_train), (x_check, y_check) = cifar10.load_data()
    y_train = to_categorical(y_train)
    y_check = to_categorical(y_check)
    x_train = x_train.astype('float32') / 255.0
    x_check = x_check.astype('float32') / 255.0
    mean_image = np.mean(x_train, axis=0)
    x_train -= mean_image
    x_check -= mean_image
    split_size = len(y_check) // 2
    x_test, y_test = x_check[split_size:], y_check[split_size:]
    x_val, y_val = x_check[:split_size], y_check[:split_size]
    if return_mean_image:
        return x_train, y_train, x_val, y_val, x_test, y_test, mean_image
    return x_train, y_train, x_val, y_val, x_test, y_test


def get_data_generator():
    return ImageDataGenerator(width_shift_range=0.2, height_shift_range=0.2, horizontal_flip=True)
