import os
import data_utils
import train_utils
import models


def test_model(test_dg, data, labels, model):
    small_num = 50
    flow = test_dg.flow(data[:small_num], labels[:small_num], batch_size=32)
    val_data = data[small_num:small_num * 2], labels[small_num:small_num * 2]
    return model.fit_generator(flow, steps_per_epoch=4, epochs=30, validation_data=val_data)


def train_model(run_dg, train_data, train_labels, val_data, val_labels, model, epochs=30):
    it_train = run_dg.flow(train_data, train_labels)
    spe = len(train_data) / 32
    val_data = (val_data, val_labels)
    run_history = model.fit_generator(it_train, steps_per_epoch=spe, epochs=epochs, validation_data=val_data)
    train_utils.save(run_history)
    return run_history


def run(use_gpu=True, testing=False):
    if use_gpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = '0'
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = '-1'
    x_train, y_train, x_val, y_val, _, _ = data_utils.get_cifar10_data()
    model = models.get_model()
    model.summary()
    datagen = data_utils.get_data_generator()
    datagen.fit(x_train)
    if testing:
        history = test_model(datagen, x_train, y_train, model)
    else:
        history = train_model(datagen, x_train, y_train, x_val, y_val, model)
    print('best val accuracy: {}'.format(max(history.history['val_acc'])))
    train_utils.plot_results(history.history)


if __name__ == '__main__':
    run()
